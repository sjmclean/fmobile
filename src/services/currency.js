export class Currency {
  constructor () {
    this._digits = 2
    this._symbol = ''
  }

  get digits () {
    return this._digits
  }

  set digits (d) {
    this._digits = d
  }

  get symbol () {
    return this._symbol
  }

  set symbol (s) {
    this._symbol = s
  }
}
