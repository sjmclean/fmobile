import { Month as month } from './data/month'
import { Account as account } from './data/account'
import { MasterCategory as masterCategory } from './data/master-category'
import { Category as category } from './data/category'
import { MonthManager as monthManager } from './data/month-manager'
import { MonthCategory as monthCategory } from './data/month-category'
import { Transaction as transaction } from './data/transaction'
import { Payee as payee } from './data/payee'
import { Currencies as currencies } from './currencies'
import { Subscription } from 'rxjs/Subscription'
import moment from 'moment'

export function DbController (budgetId, db, budget, mManager, categories, masterCategories, payees, currency) {
  const Month = month(budgetId)
  const Account = account(budgetId)
  const MasterCategory = masterCategory(budgetId)
  const Category = category(budgetId)
  const MonthManager = monthManager(budgetId)
  const MonthCategory = monthCategory()
  const Transaction = transaction(budgetId)
  const Payee = payee(budgetId)
  const Currencies = currencies()

  const currentMonth = Month.createID(new Date())

  const manager = mManager

  const accounts = mManager.accounts
  const allMonths = mManager.months

  let onBudgetAccounts = []
  let offBudgetAccounts = []
  let closedAccounts = []

  currency.symbol = Currencies[budget.currency].symbol_native
  currency.digits = Currencies[budget.currency].decimal_digits

  const _subscriptions = new Subscription()

  function filterAccounts () {
    const bySort = (a, b) => a.sort - b.sort

    onBudgetAccounts = accounts.filter(acc => acc.onBudget && !acc.closed).sort(bySort)
    offBudgetAccounts = accounts.filter(acc => !acc.onBudget && !acc.closed).sort(bySort)
    closedAccounts = accounts.filter(acc => acc.closed).sort(bySort)
  }

  function totalAccountsBalance (accounts) {
    let total = 0

    for (let i = 0; i < accounts.length; i++) {
      total += accounts[i].balance
    }

    return total
  }

  function getId (_id) {
    return _id.slice(_id.lastIndexOf('_') + 1)
  }

  function getCategoryName (id, transactionDate) {
    try {
      var monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
      ]

      if (id === 'income') {
        return 'Income for ' + monthNames[moment(transactionDate).toDate().getMonth()]
      } else if (id === 'incomeNextMonth') {
        return 'Income for ' + monthNames[moment(transactionDate).add(1, 'month').toDate().getMonth()]
      } else if (id === 'split') {
        return '☰ Multiple categories'
      }

      if (categories[id]) {
        return categories[id].name
      } else {
        return ''
      }
    } catch (err) {
      console.log(categories)
      return ''
    }
  }

  function getAccountName (id) {
    for (let i = 0; i < this.accounts.length; i++) {
      if (this.accounts[i].id === id) {
        return this.accounts[i].name
      }
    }
    return id
  }

  function getPayeeName (id) {
    return (payees[id] && payees[id].name) || id
  }

  function removeCategory (cat) {
    delete categories[cat.id]

    masterCategories[cat.masterCategory].removeCategory(cat)
  }

  function addCategory (cat) {
    categories[cat.id] = cat

    cat.subscribeMasterCategoryChange(cat => {
      removeCategory(cat)
    }, cat => {
      addCategory(cat)
    })

    let masterCat, sort

    // THIS BLOCK IS FOR BACKWARDS COMPATIBILITY 09/20/2016
    if (!cat.masterCategory) {
      Object.keys(masterCategories).forEach(catId => {
        const index = masterCategories[catId]._data.categories.indexOf(cat.id)
        if (masterCategories[catId]._data.categories &&
            index !== -1) {
          masterCat = masterCategories[catId]
          sort = index
        }
      })
    }

    if (!masterCat) {
      masterCat = masterCategories[cat.masterCategory]
    }

    // if (!masterCat) {
    //   let tmpMasterCat, currentSort = 99999;

    //   Object.keys(this.masterCategories).forEach(catId => {
    //     if (this.masterCategories[catId].sort < currentSort) {
    //       tmpMasterCat = this.masterCategories[catId];
    //       currentSort = this.masterCategories[catId].sort;
    //     }
    //   });

    //   masterCat = tmpMasterCat;
    // }

    if (typeof sort !== 'undefined') {
      cat.setMasterAndSort(masterCat.id, sort)
    } else {
      if (masterCat) {
        cat.masterCategory = masterCat.id
      } else {
        console.log(`Couldn't find master category with ID ${cat.masterCategory}!`)
      }
    }

    if (masterCat) {
      masterCat.addCategory(cat)
    } else {
      console.log(`Couldn't find master category with ID ${cat.masterCategory}!`)
    }
  }

  for (const id in categories) {
    if (categories.hasOwnProperty(id)) {
      addCategory(categories[id])
    }
  }

  const doChange = {
    masterCategory (change) {
      // look through our categories to see if it exists
      const cat = masterCategories[getId(change.id)]

      if (change.deleted) {
        if (cat) {
          delete masterCategories[getId(change.id)]

          // TODO: Replace this broadcast with Subject.
          // $scope.$broadcast('masterCategories:change')
        }
      } else {
        if (cat) {
          cat.data = change.doc
        } else {
          // Couldn't find it
          const b = new MasterCategory(change.doc)
          b.subscribe(db.put)

          masterCategories[b.id] = b

          // Add back any categories
          for (const id in categories) {
            if (categories.hasOwnProperty(id)) {
              if (categories[id].masterCategory === b.id) {
                b.addCategory(categories[id])
              }
            }
          }
        }

        // TODO: Replace this broadcast with Subject.
        // $scope.$broadcast('masterCategories:change')
      }
    },
    category (change) {
      // look through our categories to see if it exists
      const cat = categories[getId(change.id)]

      if (change.deleted) {
        if (cat) {
          removeCategory(cat)
        }
      } else {
        if (cat) {
          cat.data = change.doc
        } else {
          // Couldn't find it
          const b = new Category(change.doc)
          b.subscribe(db.put)

          addCategory(b)
          // categories[b.id] = b;
        }
      }
    },
    payee (change) {
      // look through our categories to see if it exists
      const myPayee = payees[getId(change.id)]

      if (change.deleted) {
        if (myPayee) {
          delete payees[getId(change.id)]
        }
      } else {
        if (myPayee) {
          myPayee.data = change.doc
        } else {
          // Couldn't find it
          const p = new Payee(change.doc)
          p.subscribe(db.put)

          payees[p.id] = p
        }
      }
    },
    month (change) {
      // TODO
    },
    monthCategory (change) {
      if (change.deleted) {
        const moCat = new MonthCategory(change.doc)
        const mo = mManager.getMonth(MonthManager._dateIDToDate(moCat.monthId))

        if (mo.categories[moCat.categoryId]) {
          mo.removeBudget(mo.categories[moCat.categoryId])
          mo.startRolling(moCat.categoryId)
        }
      } else {
        const moCat = new MonthCategory(change.doc)
        const mo = mManager.getMonth(MonthManager._dateIDToDate(moCat.monthId))

        if (mo.categories[moCat.categoryId]) {
          // const oldBudget = mo.categories[moCat.categoryId].budget
          mo.categories[moCat.categoryId].data = change.doc
        } else {
          moCat.subscribe(db.put)
          mo.addBudget(moCat)
          mo.startRolling(moCat.categoryId)
        }
      }
    },
    account (change) {
      for (let i = 0; i < mManager.accounts.length; i++) {
        if (mManager.accounts[i]._id === change.id) {
          if (change.deleted) {
            mManager.removeAccount(mManager.accounts[i])
          } else {
            mManager.accounts[i].data = change.doc
          }

          filterAccounts()

          return
        }
      }

      if (!change.deleted) {
        // Couldn't find it
        const acc = new Account(change.doc)
        acc.subscribe(db.put)

        mManager.addAccount(acc)

        filterAccounts()
      }
    },
    transaction (change) {
      let trans = mManager.transactions[getId(change.id)]

      if (trans) {
        if (change.deleted) {
          mManager.removeTransaction(trans)
        } else {
          if (trans.data.transfer) {
            trans.transfer = mManager.transactions[trans.data.transfer]

            if (trans.transfer) {
              trans.transfer.transfer = trans
            }
          }

          trans.data = change.doc

          trans.splits.forEach(split => {
            split.transfer = mManager.transactions[split.data.transfer]

            if (split.transfer) {
              split.transfer.transfer = split
            }
          })
        }

        return
      }

      if (!change.deleted) {
        // Couldn't find it
        trans = new Transaction(change.doc)
        trans.subscribe(db.put)

        mManager.addTransaction(trans)

        if (trans.data.transfer) {
          trans.transfer = mManager.transactions[trans.data.transfer]

          if (trans.transfer) {
            trans.transfer.transfer = trans
          }
        }

        trans.splits.forEach(split => {
          split.transfer = mManager.transactions[split.data.transfer]

          if (split.transfer) {
            split.transfer.transfer = split
          }
        })

        trans.splits.forEach(split => {
          mManager.addTransaction(split)
        })
      }
    }
  }

  filterAccounts()

  _subscriptions.add(db.$pouchChanged.subscribe((change) => {
    if (MasterCategory.contains(change.id)) {
      doChange.masterCategory(change)
    } else if (Category.contains(change.id)) {
      doChange.category(change)
    } else if (Month.contains(change.id)) {
      doChange.month(change)
    } else if (MonthCategory.contains(budgetId, change.id)) {
      doChange.monthCategory(change)
    } else if (Account.contains(change.id)) {
      doChange.account(change)
    } else if (Transaction.contains(change.id)) {
      doChange.transaction(change)
    } else if (Payee.contains(change.id)) {
      doChange.payee(change)
    }
  }))

  function destroy () {
    _subscriptions.unsubscribe()
  }

  return {
    currentMonth,
    budgetId,
    budget,
    manager,
    accounts,
    allMonths,
    payees,
    categories,
    masterCategories,
    getCategoryName,
    getAccountName,
    getPayeeName,
    totalAccountsBalance,
    onBudgetAccounts,
    offBudgetAccounts,
    closedAccounts,
    destroy
  }
}
