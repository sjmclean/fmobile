import moment from 'moment'

export function DateSettings () {
  function getDateFormat () {
    return localStorage.getItem('dateFormat')
  }

  function setDateFormat (format) {
    localStorage.setItem('dateFormat', format)
  }

  function dateDisplay (date) {
    var currDay = date.getDate()
    var currMonth = date.getMonth() + 1
    var currYear = date.getFullYear()
    var currFormat = getDateFormat()

    if (currFormat === 'YY/MM/DD') {
      return String(currYear).slice(2, 4) + '/' + currMonth + '/' + currDay
    }

    if (currFormat === 'DD/MM/YY') {
      return currDay + '/' + currMonth + '/' + String(currYear).slice(2, 4)
    }

    if (currFormat === 'MM/DD/YY') {
      return currMonth + '/' + currDay + '/' + String(currYear).slice(2, 4)
    }
  }

  return {
    getDateFormat,
    setDateFormat,
    dateDisplay
  }
}

export function DateConverter () {
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]

  const shortMonthNames = [
    'Dec',
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ]

  function BudgetMonth (date) {
    const month = monthNames[moment(date).toDate().getMonth()]
    const year = String(moment(date).toDate().getFullYear())
    return month + ' ' + year
  }

  function ShortMonth (date, offset = 0) {
    const month = shortMonthNames[moment(date).toDate().getMonth() + offset + 1]
    return month
  }

  return {
    BudgetMonth,
    ShortMonth
  }
}
