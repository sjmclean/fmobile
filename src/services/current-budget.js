import { Subject } from 'rxjs/Subject'
import { DbController } from './db-controller'

export function CurrentBudget (currency) {
  let id
  let manager
  let dbCtrl

  const $budgetOpened = new Subject()
  const $budgetClosed = new Subject()

  function loadBudget (budgetId, db) {
    unloadBudget()

    id = budgetId
    manager = db.budget(id)

    return Promise.all([
      manager.budget(),
      manager.categories.all(),
      manager.masterCategories.all(),
      manager.payees.all(),
      db.budgets.get(id)
    ]).then(([monthManager, categories, masterCategories, payees, budget]) => {
      monthManager.propagateRolling(Object.keys(categories))
      dbCtrl = new DbController(id, db, budget, monthManager, categories, masterCategories, payees, currency)
      $budgetOpened.next(dbCtrl.budget.name)
      this.monthMemory = new Date()
      return dbCtrl
    }).catch(e => {
      throw e
    })
  }

  function unloadBudget () {
    if (dbCtrl !== undefined) {
      dbCtrl.destroy()
      dbCtrl = undefined
      $budgetClosed.next(null)
    }
  }

  function budgetMgr () {
    return manager
  }

  function budgetId () {
    return id
  }

  return {
    budgetId,
    budgetMgr,
    dbCtrl,
    loadBudget,
    unloadBudget,
    $budgetOpened,
    $budgetClosed
  }
}
