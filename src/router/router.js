import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home/home'
import Accounts from '@/components/accounts/accounts'
import Account from '@/components/accounts/account'
import Budget from '@/components/budget/budget'
import Settings from '@/components/settings/settings'
import Theme from '@/components/settings/theme'
import Login from '@/components/login/login'
import About from '@/components/about/about'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/:budgetId/accounts',
      name: 'Accounts',
      component: Accounts
    },
    {
      path: '/:budgetId/account/:accountId',
      name: 'Account',
      component: Account
    },
    {
      path: '/:budgetId/budget',
      name: 'Budget',
      component: Budget
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/settings/theme',
      name: 'Theme',
      component: Theme
    },
    {
      path: '/about',
      name: 'About',
      component: About
    }
  ]
})

export default router
